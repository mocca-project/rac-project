Rails.application.routes.draw do
  resources :tutorials
  resources :tohelps
  resources :locations
  resources :roles
  resources :sessions
  resources :posttests
  resources :favoriteposts
  resources :users
  resources :comments
  resources :posts do
    resources :comments
    resources :tohelps
  end
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'
  put 'users' => 'users#update'
  delete 'image' => 'posts#destroyImage'
  get 'verifyusers' => 'users#verifyUsers'
  get 'myposts' => 'posts#showMypost'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
