class Tutorial < ApplicationRecord

  def self.get_tutorial(params)
    p "1==========="
    p params[:keyword]
    p "2==========="
    if !(params[:keyword].blank?)
      @tutorial = Tutorial.where("title = ?", params[:keyword])
      return @tutorial
    else
      @tutorial = Tutorial.all
      return @tutorial
    end
  end

  def self.new_tutorial(params)
    @tutorial = Tutorial.new
    @tutorial.title = params[:tutorial][:title]
    @tutorial.description = params[:tutorial][:description]
    if @tutorial.save
      return { message: "Create tutorial successfully.", data: @tutorial }
    end
  end

  def self.delete_tutorial(params)
      @tutorial = Tutorial.find(params[:id])

      if @tutorial.id = params[:id]
        @tutorial.destroy
        return {message: "Delete tutorial Successfully.", data: @tutorial }
      else
        return {error: "Can't delete this tutorial post."}
      end
  end

end
