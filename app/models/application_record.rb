class ApplicationRecord < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  self.abstract_class = true

  def self.decode_64(image)
    @decoded_file = Base64.decode64(image)        
    @filename = "image_" + Time.now.strftime("%Y%m%d%I%M%S") + ".jpg"
    @tmp_file = Tempfile.new(@filename)        # This creates an in-memory file 
    @tmp_file.binmode                          # This helps writing the file in binary mode.
    @tmp_file.write @decoded_file					
    @tmp_file.rewind()
    image = @tmp_file
    return image
  end

  #Get Image
  def get_image(id)    
    img = !self.image.attached? ? nil : rails_blob_path(self.image,only_path:true)
  end  

  #Get Images
  def get_images(id)
    imgs = !self.image.attached? ? nil : self.image.map{ |img| rails_blob_path(img,only_path:true) }
    ids = !self.image.attached? ? nil : self.image.map{ |img| img.id}
    results = []
    if !(imgs.blank?)
      imgs.each_with_index do |img, i|
        results << {
          id: ids[i],
          image: img
        }
      end
    else 
      results = nil
    end
    results
  end 

end
