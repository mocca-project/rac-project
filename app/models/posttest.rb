class Posttest < ApplicationRecord
  has_one_attached :image
  def self.new_post(params)
    @posttest = Posttest.new(params)
    if @posttest.save
          @posttest.image.attach(params[:image])
        return { message: "Posttest Complete.", data: @posttest }        
    else
      return { error: @posttest.errors }
    end
  end
end
