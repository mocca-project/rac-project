class Favoritepost < ApplicationRecord
  include Rails.application.routes.url_helpers
  has_one :user
  has_one :post

  def self.get_my_favoritepost(user_id)
    data = Favoritepost.where("user_id = ?", user_id)
    dataa = []
    numchk = 0 
    data.each do |dt|
      if !(dt["id"] == numchk)
        pitem = Post.find(dt["post_id"])
        litem = Location.find(dt["post_id"])
        uitem = User.find(pitem["user_id"])

        if (pitem["identify"] == true)
        dataa << {
          id: dt["id"],
          user_id: dt["user_id"],
          post: {
            post_id: pitem["id"],
            title: pitem["title"],
            description: pitem["description"],
            type_post: pitem["type_post"],
            status: pitem["status"],
            nature: pitem["nature"],
            identify: pitem["identify"],
            image: pitem.get_images(dt["post_id"]),
            creater: {
              user_id: uitem["id"],
              name: uitem["name"],
              image: uitem.get_image(dt["user_id"]),
              role_id: uitem["role_id"]
            },
            location: {
              id: litem["id"],
              title: litem["title"],
              latitude: litem["latitude"],
              longitude: litem["longitude"],
              created_at: litem["created_at"],
              updated_at: litem["updated_at"]
              }
          },
        created_at: dt["created_at"],
        updated_at: dt["updated_at"],
        }
        elsif (pitem["identify"] == false)
        dataa << {
          id: dt["id"],
          user_id: dt["user_id"],
          post: {
            post_id: pitem["id"],
            title: pitem["title"],
            description: pitem["description"],
            type_post: pitem["type_post"],
            status: pitem["status"],
            nature: pitem["nature"],
            identify: pitem["identify"],
            image: pitem.get_images(dt["post_id"]),
            creater: {
              user_id: uitem["id"],
              name: "Incognito",
              image: "",
              role_id: uitem["role_id"]
            },
            location: {
              id: litem["id"],
              title: litem["title"],
              latitude: litem["latitude"],
              longitude: litem["longitude"],
              created_at: litem["created_at"],
              updated_at: litem["updated_at"]
              }
          },
        created_at: dt["created_at"],
        updated_at: dt["updated_at"],
        }
        end
        
      end
    end
    return dataa
  end

  def self.new_favoritepost(params, user_id)
    @favoritepost = Favoritepost.new
    @favoritepost.post_id = params[:favoritepost][:post_id]
    @favoritepost.user_id = user_id
    if @favoritepost.save
      return { message: "Create favoritepost successfully.", data: @favoritepost }
    end
  end

  def self.delete_favoritepost(params)
    if !(params[:user_id].blank?)
      @favoritepost = Favoritepost.find(params[:id])
      if @favoritepost.user_id == params[:user_id]
        @favoritepost.destroy
        return {message: "Delete favorite post Successfully.", data: @favoritepost }
      else
        return {error: "Can't delete this favorite post."}
      end
    else
      return {error: "Don't have user_id."}
    end
  end
end
