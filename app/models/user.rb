class User < ApplicationRecord
  include Rails.application.routes.url_helpers
  belongs_to :role
  has_many :post
  has_many :comment
  has_many :favoritepost
  has_many :tohelp
  has_many :sessions, dependent: :destroy
  has_one_attached :image
  has_one_attached :verify_image
  validates :username, length: { minimum: 5 }, presence: true, uniqueness: true
  validates :password, length: { minimum: 5 }
  validates :name, length: { minimum: 2 }
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP } , uniqueness: true
  validates :phone, length: { maximum: 10 ,minimum: 10}, uniqueness: true

  def as_json(options = {})
    super(:except => [:password, :reset_password_token, :reset_password_sent_at]).merge!({
            image: !self.image.attached? ? nil : rails_blob_path(self.image,only_path:true),
            verify_image: !self.verify_image.attached? ? nil : rails_blob_path(self.verify_image,only_path:true)
        })
  end

  def self.get_user(params)
    if !(params[:keyword].blank?)
        if (params[:keyword] == "1") #user
            users = User.where("role_id = 1")
            return users
        elsif (params[:keyword] == "2") #volunteer
            users = User.where("role_id = 2")
            return users
        elsif (params[:keyword] == "3") #admin
            users = User.where("role_id = 3")
            return users
        else
            return { message: "not have role id"}
        end
    else
        @user = User.all.order("id ASC")
        return @user
    end
    
  end

  def self.get_verifyuser()
    @user = User.where("verify = ?", true).order("id ASC")
    return @user
  end

  def self.new_register(params)        
        params[:username] = params[:username].downcase
        params[:password] = Digest::SHA2.new(512).hexdigest(params[:password])

        #check same username & password
        chkuser = User.where("username = ?", params[:username])
        chkemail = User.where("email = ?", params[:email])

        if (chkuser.blank? && chkemail.blank?)
            # return { message: "not same" }
            if !(params[:image].blank?)
            begin
                # base64
                params[:image] = User.decode_64(params[:image])
                @filename = "image_" + Time.now.strftime("%Y%m%d%I%M%S")

                @user = User.new(params)
                if @user.save
                    @user.image.attach(io:File.open(params[:image]), filename: @filename)
                    return { message: "Sign up Complete.", user_info: @user }
                else
                    return { error: @user.errors }
                end
            rescue
                # file
                @user = User.new(params)
                if @user.save
                    return { message: "Sign up Complete.", user_info: @user }
                else
                    return { error: @user.errors }
                end
            end
            else
                # none image
                @user = User.new(params)
                if @user.save
                    @user.image.attach(io:File.open('./user.png'), filename: 'user.png', content_type: 'image/png')
                    return { message: "Sign up Complete.", user_info: @user }
                else
                    return { error: @user.errors }
                end
            end
        else
            return { message: "Same username or email" }
        end
        
    end

    def self.update_user(params,user_id)
        if !(params[:user][:user_id].blank?) # update user for admin, send id
            chk = User.where("id = ? ", user_id).first
            if chk[:role_id] == 3
                @user = User.where("id = ? ", params[:user][:user_id]).first
                if !(params[:user][:image].blank?)
                    begin
                        # profile image
                        params[:user][:image] = User.decode_64(params[:user][:image])
                        @filename = "image_" + Time.now.strftime("%Y%m%d%I%M%S")

                        params[:user][:id] = params[:user][:user_id]
                        if @user.update params.require(:user).permit(:id, :name, :email, :phone, :address, :gender, :position, :role_id, :image, :verify, :verify_image)
                            @user.image.attach(io:File.open(params[:user][:image]), filename: @filename)
                            return { message: "Update successfully.", user_info: @user }
                        end
                    rescue
                        @user.update params.require(:user).permit(:id, :name, :email, :phone, :address, :gender, :position, :role_id, :image, :verify, :verify_image)
                        return { message: "Update successfully.", user_info: @user }
                    end
                elsif !(params[:user][:verify_image].blank?)
                    # verify image
                    params[:user][:verify_image] = User.decode_64(params[:user][:verify_image])
                    @verify_filename = "image_" + Time.now.strftime("%Y%m%d%I%M%S")
                    @user.verify_image.attach(io:File.open(params[:user][:verify_image]), filename: @verify_filename)
                    @user.update params.require(:user).permit(:id, :name, :email, :phone, :address, :gender, :position, :role_id, :image, :verify, :verify_image)
                    return { message: "Update successfully.", user_info: @user }

                elsif @user.update params.require(:user).permit(:id, :name, :email, :phone, :address, :gender, :position, :role_id, :image, :verify, :verify_image)
                    return { message: "Update successfully.",user_info: @user }
                else
                    return {error: @user.errors }
                end
            end
        else # for user
            #for User check id with token
            @user = User.where("id = ? ", user_id).first
            params[:user][:id] = user_id
            if !(params[:user][:password_new].blank?) # update password
                oldPass = @user[:password]
                chkPass = Digest::SHA2.new(512).hexdigest(params[:user][:password])
                if oldPass == chkPass
                    newPass = Digest::SHA2.new(512).hexdigest(params[:user][:password_new])
                    params[:user][:password] = newPass
                    if @user.update params.require(:user).permit(:id, :password)
                        return { message: "Update password successfully.",
                            user_info: @user }
                    else
                        return {error: @user.errors }
                    end
                else
                    return {error: "Incorrect password."}
                end
            else # update profile
                if !(params[:user][:image].blank?)
                    begin
                        # profile image
                        params[:user][:image] = User.decode_64(params[:user][:image])
                        @filename = "image_" + Time.now.strftime("%Y%m%d%I%M%S")

                        if @user.update params.require(:user).permit(:id, :name, :email, :phone, :address, :gender, :position, :role_id, :image, :verify, :verify_image)
                            @user.image.attach(io:File.open(params[:user][:image]), filename: @filename)
                            return { message: "Update successfully.", user_info: @user }
                        else
                            return {error: @user.errors }
                        end
                    rescue
                        @user.update params.require(:user).permit(:id, :name, :email, :phone, :address, :gender, :position, :role_id, :image, :verify, :verify_image)
                        return { message: "Update successfully.", user_info: @user }
                    end
                elsif !(params[:user][:verify_image].blank?)
                    # verify image
                    params[:user][:verify_image] = User.decode_64(params[:user][:verify_image])
                    @verify_filename = "image_" + Time.now.strftime("%Y%m%d%I%M%S")
                    @user.verify_image.attach(io:File.open(params[:user][:verify_image]), filename: @verify_filename)
                    @user.update params.require(:user).permit(:id, :name, :email, :phone, :address, :gender, :position, :role_id, :image, :verify, :verify_image)
                    return { message: "Update successfully.", user_info: @user }
                    
                elsif @user.update params.require(:user).permit(:id, :name, :email, :phone, :address, :gender, :position, :role_id, :image, :verify, :verify_image)
                    return { message: "Update successfully.",user_info: @user }
                end
                
            end
        end
    end

    # def self.all_user(params)
    #     user = User.all.order(params[:order] ||= "id ASC").offset(params[:offset] ||= 0).limit(params[:limit] ||= 10)
    #     return user.as_json
    #     # @users = User.all
    #     # return { user_info: @users }
    # end

    def self.delete(params)
        if !(params[:user_id].blank?)
            @user = User.find(params[:id])
            @user.destroy
            return {message: "Delete Successfully", data: @user }
        else
            return {error: "Don't have user_id."}
        end
    end

end
