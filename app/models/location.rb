class Location < ApplicationRecord
  belongs_to :post

  def self.update_location(params)
    @location = Location.where("post_id = ?",params[:id]).first
     
    if @location
      @location.title = params[:location][:title]
      @location.latitude = params[:location][:latitude]
      @location.longitude = params[:location][:longitude]
      if @location.save
          # dataa = []
          # pitem = Post.find(params[:post][:id])
          # dataa << {
          #   message: "Update post successfully.",
          #   data: {
          #     id: params[:post][:id],
          #     title: params[:post][:title],
          #     description: params[:post][:description],
          #     type_post: params[:post][:type_post],
          #     status: params[:post][:status],
          #     nature: params[:post][:nature],
          #     images: pitem.get_images(params[:post][:id]),
          #     location: {
          #       id: params[:location][:id],
          #       title: params[:location][:title],
          #       latitude: params[:location][:latitude],
          #       longitude: params[:location][:longitude]
          #     }
          #   }
          # }
 
          # return dataa
      else
        return {error: "Can't update location."}
      end
    else
      return {error: "Can't find location."}
    end
    
  end
end