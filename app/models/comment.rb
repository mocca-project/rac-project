class Comment < ApplicationRecord
  belongs_to :post
  belongs_to :user
  has_one_attached :image
  has_one_attached :image_user

  def as_json(options = {})
    super(:except => [:created_at,:updated_at]).merge!({
      name: self.user.name,
      role_id: self.user.role_id,
      image_user: !self.user.image.attached? ? nil : rails_blob_path(self.user.image,only_path:true),
      image: !self.image.attached? ? [] : rails_blob_path(self.image,only_path:true),
      created_at: self.created_at,
      updated_at: self.updated_at
    })
  end

  def self.get_comment(params)
    @comment = Comment.where("post_id = ?", params[:post_id]).order("id DESC")
    return @comment
  end

  def self.new_comment(params, user_id)
    if !(params[:comment][:image].blank?) #have img
      begin
        params[:comment][:image] = Post.decode_64(params[:comment][:image])
        @filename = "image_" + Time.now.strftime("%Y%m%d%I%M%S")
        @comment = Comment.new
        @comment.user_id = user_id
        @comment.post_id = params[:comment][:post_id]
        @comment.description = params[:comment][:description]
        if @comment.save
          @comment.image.attach(io:File.open(params[:comment][:image]), filename: @filename)
          return { message: "Create comment successfully.", data: @comment.as_json }
        else
          return {error: @comment.errors}
        end
      rescue
        @comment = Comment.new
        @comment.user_id = user_id
        @comment.post_id = params[:comment][:post_id]
        @comment.description = params[:comment][:description]
        @comment.image = params[:comment][:image]
        if @comment.save
          return { message: "Create comment successfully.", data: @comment.as_json }
        else
          return {error: @comment.errors}
        end
      end
    else #none img
      @comment = Comment.new
      @comment.user_id = user_id
      @comment.post_id = params[:comment][:post_id]
      @comment.description = params[:comment][:description]
      if @comment.save
          return { message: "Create comment successfully.", data: @comment.as_json }
        else
          return {error: @comment.errors}
        end
    end
  end

  def self.update_comment(params, user_id)
    if !(params[:comment].blank?)
      chkuser = User.find(user_id)    
      @comment = Comment.find(params[:id])
      if @comment.user_id == user_id || chkuser.role_id == 3 #update comment (user, admin)
        if !(params[:comment][:image].blank?)
          begin
            params[:comment][:image] = Post.decode_64(params[:comment][:image])
            @filename = "image_" + Time.now.strftime("%Y%m%d%I%M%S")
            @comment.description = params[:comment][:description]
            if @comment.save
              @comment.image.attach(io:File.open(params[:comment][:image]), filename: @filename)
              return { message: "Update comment successfully.", data: @comment.as_json }
            end
          rescue
            @comment.description = params[:comment][:description]
            @comment.image = params[:comment][:image]
            if @comment.save
              return { message: "Update comment successfully.", data: @comment.as_json }
            end
          end
        else
           @comment.description = params[:comment][:description]
            if @comment.save
              return { message: "Update comment successfully.", data: @comment.as_json }
            end
        end
      else
        return {error: "Can't update this comment."}
      end
    end
  end

  def self.delete_comment(params)
    if !(params[:user_id].blank?)
      @user = User.find(params[:user_id])
      @post = Post.find(params[:post_id])
      @comment = Comment.find(params[:id])
      if @comment.user_id == params[:user_id] || @user.role_id == 3 #delete comment (user, admin)
        @comment.destroy
        return {message: "Delete comment Successfully.", data: @comment }
      else
        return {error: "Can't delete this comment."}
      end
    else
      return {error: "Don't have user_id."}
    end
  end

end
