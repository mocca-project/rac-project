class Post < ApplicationRecord
  include Rails.application.routes.url_helpers
  require "net/http"
  belongs_to :user
  has_many :comment
  has_many :favoritepost, dependent: :destroy
  has_one :location, dependent: :destroy
  has_many :tohelp, dependent: :destroy
  has_many_attached :image

  def as_json(options = {})
    super(:except => [:created_at,:updated_at]).merge!({
      image: !self.image.attached? ? [] : rails_blob_path(self.image, only_path:true)
    })
  end

  def self.get_all_post(params)
    # @posts = Post.all.order("id ASC")
    data = Post.all.limit(params[:limit] ||= 10).order(params[:order] ||= "id DESC").offset(params[:offset] ||= 0)

    dataa = []
    numchk = 0
    data.each do |dt|
      if !(dt["id"] == numchk)
        pitem = Post.find(dt["id"])
        litem = Location.find(dt["id"])
        uitem = User.find(dt["user_id"])

        if (dt["identify"] == true)
          dataa << {
            id: dt["id"],
            title: dt["title"],
            description: dt["description"],
            type_post: dt["type_post"],
            status: dt["status"],
            nature: dt["nature"],
            identify: dt["identify"],
            images: pitem.get_images(dt["id"]),
            creater: {
              user_id: uitem["id"],
              name: uitem["name"],
              image: uitem.get_image(dt["user_id"]),
              role_id: uitem["role_id"]
            },
            created_at: dt["created_at"],
            updated_at: dt["updated_at"],
            location: {
              id: litem["id"],
              title: litem["title"],
              latitude: litem["latitude"],
              longitude: litem["longitude"],
              created_at: litem["created_at"],
              updated_at: litem["updated_at"]
            }
          }
        elsif (dt["identify"] == false)
          dataa << {
            id: dt["id"],
            title: dt["title"],
            description: dt["description"],
            type_post: dt["type_post"],
            status: dt["status"],
            nature: dt["nature"],
            identify: dt["identify"],
            images: pitem.get_images(dt["id"]),
            creater: {
              user_id: uitem["id"],
              name: "Incognito",
              image: "",
              role_id: uitem["role_id"]
            },
            created_at: dt["created_at"],
            updated_at: dt["updated_at"],
            location: {
              id: litem["id"],
              title: litem["title"],
              latitude: litem["latitude"],
              longitude: litem["longitude"],
              created_at: litem["created_at"],
              updated_at: litem["updated_at"]
            }
          }
        end
      end
    end

    return dataa
  end

  def self.get_my_post(user_id, params)
    post = Post.where("user_id = ?", user_id).limit(params[:limit] ||= 10).order(params[:order] ||= "id DESC").offset(params[:offset] ||= 0)
    data = post

    dataa = []
    numchk = 0
    data.each do |dt|
      if !(dt["id"] == numchk)
        pitem = Post.find(dt["id"])
        litem = Location.find(dt["id"])
        uitem = User.find(dt["user_id"])
        dataa << {
        id: dt["id"],
        title: dt["title"],
        description: dt["description"],
        type_post: dt["type_post"],
        status: dt["status"],
        nature: dt["nature"],
        identify: dt["identify"],
        images: pitem.get_images(dt["id"]),
        creater: {
          user_id: uitem["id"],
          name: uitem["name"],
          image: uitem.get_image(dt["user_id"]),
          role_id: uitem["role_id"]
        },
        created_at: dt["created_at"],
        updated_at: dt["updated_at"],
        location: {
          id: litem["id"],
          title: litem["title"],
          latitude: litem["latitude"],
          longitude: litem["longitude"],
          created_at: litem["created_at"],
          updated_at: litem["updated_at"]
        }
      }
      end
    end

    return dataa
  end

  def self.get_post(post,user_id,params)
    my_comment = Comment.where("post_id = ?", post[:id]).order("id DESC")
    data = post
    pitem = Post.find(post[:id])
    litem = Location.find(post[:id])
    uitem = User.find(post[:user_id])

    if (post[:identify] == true)
      dataa = {
        id: post[:id],
        title: post[:title],
        description: post[:description],
        type_post: post[:type_post],
        status: post[:status],
        nature: post[:nature],
        identify: post[:identify],
        images: pitem.get_images(post[:id]),
        creater: {
          user_id: uitem["id"],
          name: uitem["name"],
          image: uitem.get_image(uitem["user_id"]),
          role_id: uitem["role_id"]
        },
        created_at: post[:created_at],
        updated_at: post[:updated_at],
        location: {
          id: litem["id"],
          title: litem["title"],
          latitude: litem["latitude"],
          longitude: litem["longitude"],
          created_at: litem["created_at"],
          updated_at: litem["updated_at"]
        }, 
          comments: my_comment
      }
    elsif (post[:identify] == false)
      dataa = {
        id: post[:id],
        title: post[:title],
        description: post[:description],
        type_post: post[:type_post],
        status: post[:status],
        nature: post[:nature],
        identify: post[:identify],
        images: pitem.get_images(post[:id]),
        creater: {
          user_id: uitem["id"],
          name: "Incognito",
          image: "",
          role_id: uitem["role_id"]
        },
        created_at: post[:created_at],
        updated_at: post[:updated_at],
        location: {
          id: litem["id"],
          title: litem["title"],
          latitude: litem["latitude"],
          longitude: litem["longitude"],
          created_at: litem["created_at"],
          updated_at: litem["updated_at"]
        }, 
          comments: my_comment
      }
    end

    return dataa
  end

  def self.new_post(params,user_id)
    if !(params[:post][:user_id].blank?)
      user_id = params[:post][:user_id]
    end
    if !(params[:location].blank?)
      if !(params[:post][:image].blank?)
        begin # base 64
          imgdata = []
          imgname = []
          number = 0
          params[:post][:image].each do |img|
            number = number + 1
            imgdata << Post.decode_64(img)
            imgname << "image_" + Time.now.strftime("%Y%m%d%I%M%S") + "_" + number.to_s
          end

          @post = Post.new(
            user_id: user_id,
            title: params[:post][:title],
            description: params[:post][:description],
            type_post: params[:post][:type_post],
            status: params[:post][:status],
            nature: params[:post][:nature],
            identify: params[:post][:identify]
          )
          if @post.save
            imgdata.each do |img|
              @post.image.attach(io:File.open(img), filename: imgname)
            end

            @location = Location.new
            @location.post_id = @post.id
            @location.title = params[:location][:title]
            @location.latitude = params[:location][:latitude]
            @location.longitude = params[:location][:longitude]
            if @location.save
              dataa = []
              pitem = Post.find(@post.id)
              post_notification = Post.create_notification(params)

              dataa << {
                message: "Create post successfully.",
                data: {
                  id: @post.id,
                  title: params[:post][:title],
                  description: params[:post][:description],
                  type_post: params[:post][:type_post],
                  status: params[:post][:status],
                  nature: params[:post][:nature],
                  identify: params[:post][:identify],
                  images: pitem.get_images(@post.id),
                  location: {
                    id: params[:location][:id],
                    title: params[:location][:title],
                    latitude: params[:location][:latitude],
                    longitude: params[:location][:longitude],
                    created_at: params[:location][:created_at],
                    updated_at: params[:location][:updated_at]
                  }
                }
              }
              return dataa
            else
              return {error: @location.errors}
            end
          else
            return {error: @post.errors}
          end
        rescue # file
          @post = Post.new
          @post.user_id = user_id
          @post.title = params[:post][:title]
          @post.description = params[:post][:description]
          @post.type_post = params[:post][:type_post]
          @post.status = params[:post][:status]
          @post.nature = params[:post][:nature]
          @post.image = params[:post][:image]
          @post.identify = params[:post][:identify]

          if @post.save
            @location = Location.new
            @location.post_id = @post.id
            @location.title = params[:location][:title]
            @location.latitude = params[:location][:latitude]
            @location.longitude = params[:location][:longitude]
            if @location.save
              dataa = []
              pitem = Post.find(@post.id)
              post_notification = Post.create_notification(params)

              dataa << {
                message: "Create post successfully.",
                data: {
                  id: @post.id,
                  title: params[:post][:title],
                  description: params[:post][:description],
                  type_post: params[:post][:type_post],
                  status: params[:post][:status],
                  nature: params[:post][:nature],
                  identify: params[:post][:identify],
                  images: pitem.get_images(@post.id),
                  location: {
                    id: params[:location][:id],
                    title: params[:location][:title],
                    latitude: params[:location][:latitude],
                    longitude: params[:location][:longitude],
                    created_at: params[:location][:created_at],
                    updated_at: params[:location][:updated_at]
                  }
                }
              }
              return dataa
            else
              return {error: @location.errors}
            end
          else
            return {error: @post.errors}
          end

        end
      else # none image
        @post = Post.new
          @post.user_id = user_id
          @post.title = params[:post][:title]
          @post.description = params[:post][:description]
          @post.type_post = params[:post][:type_post]
          @post.status = params[:post][:status]
          @post.nature = params[:post][:nature]
          @post.identify = params[:post][:identify]
          if @post.save
            @post.image.attach(io: File.open('./post.png'), filename: 'post.png', content_type: 'image/png')

            @location = Location.new
            @location.post_id = @post.id
            @location.title = params[:location][:title]
            @location.latitude = params[:location][:latitude]
            @location.longitude = params[:location][:longitude]
            if @location.save
              dataa = []
              pitem = Post.find(@post.id)
              post_notification = Post.create_notification(params)

              dataa << {
                message: "Create post successfully.",
                data: {
                  id: @post.id,
                  title: params[:post][:title],
                  description: params[:post][:description],
                  type_post: params[:post][:type_post],
                  status: params[:post][:status],
                  nature: params[:post][:nature],
                  identify: params[:post][:identify],
                  images: pitem.get_images(@post.id),
                  location: {
                    id: params[:location][:id],
                    title: params[:location][:title],
                    latitude: params[:location][:latitude],
                    longitude: params[:location][:longitude],
                    created_at: params[:location][:created_at],
                    updated_at: params[:location][:updated_at]
                  }
                }
              }
              return dataa
            else
              return {error: @location.errors}
            end
          else
            return {error: @post.errors}
          end
      end
    else
      return { message: "Location is null" }
    end
  end

  def self.create_notification(posts)
    params = {"app_id" => "9022bdcb-16a4-4f23-a3ed-04d3fccb16dc",
              "contents" => {"en" => posts[:post][:title]},
              "included_segments" => ["Volunteer Users"]}
    uri = URI.parse('https://onesignal.com/api/v1/notifications')
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    request = Net::HTTP::Post.new(uri.path,
                'Content-Type'  => 'application/json;charset=utf-8',
                'Authorization' => "Basic MzY2NWUyZTktMDgzYy00ODQ2LWJkZDAtNWUwMGJkMTE1NDEw")
    request.body = params.as_json.to_json
    response = http.request(request) 
    puts response.body
    return nil
  end

  def self.update_post(params, user_id)
    @user = User.find(user_id)
    post = Post.find(params[:id])
    if post.user_id == user_id || @user.role_id == 2 || @user.role_id == 3 #update post (user, volunteer, admin)
      @post = Post.updatepost(params)
      litem = Location.find(params[:id])
      dataa = []
      dataa << {
        message: "๊Update post successfully.",
        data: {
          id: params[:id],
          title: params[:post][:title],
          description: params[:post][:description],
          type_post: params[:post][:type_post],
          status: params[:post][:status],
          nature: params[:post][:nature],
          identify: params[:post][:identify],
          images: post.get_images(params[:id]),
          location: {
            id: litem[:id],
            title: params[:location][:title],
            latitude: params[:location][:latitude],
            longitude: params[:location][:longitude]
          }
        }
      }
      return dataa
    else
      return {error: "Can't updated this form."}
    end
  end

  def self.updatepost(params)
    @post = Post.find(params[:id])
    if !(params[:post][:image].blank?)
      begin
        imgdata = []
        imgname = []
        number = 0
        params[:post][:image].each do |img|
          number = number + 1
          imgdata << Post.decode_64(img)
          imgname << "image_" + Time.now.strftime("%Y%m%d%I%M%S") + "_" + number.to_s
        end
        # params[:post][:image] = User.decode_64(params[:post][:image])
        # @filename = "image_" + Time.now.strftime("%Y%m%d%I%M%S")
        @post.title = params[:post][:title]
        @post.description = params[:post][:description]
        @post.type_post = params[:post][:type_post]
        @post.status = params[:post][:status]
        @post.nature = params[:post][:nature]
        
        pitem = Post.find(params[:id])
        images = pitem.get_images(params[:id])
        if !(images.blank?)
          images.each do |img|
            @post.image.find_by_id(img[:id]).purge
          end
        end

        if @post.save
          imgdata.each do |img|
            @post.image.attach(io:File.open(img), filename: imgname)
          end
          # @post.image.attach(io:File.open(params[:post][:image]), filename: @filename)
        else
          return {error: @post.errors}
        end
      rescue
        pitem = Post.find(params[:id])
        images = pitem.get_images(params[:id])
        if !(images.blank?)
          images.each do |img|
            @post.image.find_by_id(img[:id]).purge
          end
        end

        @post.title = params[:post][:title]
        @post.description = params[:post][:description]
        @post.type_post = params[:post][:type_post]
        @post.status = params[:post][:status]
        @post.nature = params[:post][:nature]
        @post.image = params[:post][:image]
        if @post.save

        else
          return {error: @post.errors}    
        end
      end
    else
      @post.title = params[:post][:title]
      @post.description = params[:post][:description]
      @post.type_post = params[:post][:type_post]
      @post.status = params[:post][:status]
      @post.nature = params[:post][:nature]
      if @post.save

      else
        return {error: @post.errors}    
      end
    end
    if !(params[:location].blank?)
      Location.update_location(params)
    end
    # return { message: "Updated Successfully.", data: @post.as_json }
  end

  def self.delete_post(params)
    if !(params[:user_id].blank?)
      @user = User.find(params[:user_id])
      @post = Post.find(params[:id])
      if @post.user_id == params[:user_id] || @user.role_id == 2 || @user.role_id == 3 #delete post (user, volunteer, admin)
        @post.destroy
        return { message: "Delete Successfully.", data: @post.as_json }
      else
        return { message: "Can't delete this post." }
      end
    end
  end

  def self.delete_image(params)
    if !(params[:user_id].blank?)
      params[:image].each do |img|
        @user = User.find(params[:user_id])
        @post = Post.find(img[:post_id])
        if @post.user_id == params[:user_id] || @user.role_id == 3
          @post.image.find_by_id(img[:image_id]).purge
          @post.save
        end
      end
      return {message: "Delete image Successfully."}
    else
      return {error: "Don't have user_id."}
    end
  end

end
