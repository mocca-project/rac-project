class Tohelp < ApplicationRecord
  include Rails.application.routes.url_helpers
  has_one :user
  has_one :post


  def self.get_tohelp(params, user_id)
      data = Tohelp.where("post_id = ?", params[:post_id]).order("id DESC")
      dataa = []
      numchk = 0
      data.each do |dt|
        if !(dt["id"] == numchk)
          uitem = User.find(dt["user_id"])
          dataa << {
            id: dt["id"],
            post_id: dt["post_id"],
            user_id: dt["user_id"],
            name: uitem["name"],
            role_id: uitem["role_id"],
            image: uitem.get_image(dt["user_id"]),
            created_at: dt["created_at"],
            updated_at: dt["updated_at"],
          }
        end
      end
    return dataa

  end

  def self.new_tohelp(params)
    if !(params[:user_id].blank?)
      if !(params[:post_id].blank?)
        user = User.find(params[:user_id])
        if (user[:role_id] == 2 || user[:role_id] == 3)
          @tohelp = Tohelp.new
          @tohelp.post_id = params[:post_id]
          @tohelp.user_id = params[:user_id]

          if @tohelp.save
            return { message: "Create tohelp successfully.", data: @tohelp }
          else
            return { error: @tohelp.error }
          end
        else
          return { message: "You Not Volunteer." }
        end
        
      else
        return {error: "Don't have post_id."}
      end
    else
      return {error: "Don't have user_id."}
    end
  end

  def self.delete_tohelp(params)
    if !(params[:user_id].blank?)
      @tohelp = Tohelp.find(params[:id])
      if @tohelp.user_id == params[:user_id]
        @tohelp.destroy
        return {message: "Delete tohelp Successfully.", data: @tohelp }
      else
        return {error: "Can't delete this tohelp."}
      end
    else
      return {error: "Don't have user_id."}
    end
  end



end

