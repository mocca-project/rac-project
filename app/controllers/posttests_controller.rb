class PosttestsController < ApplicationController
  before_action :set_posttest, only: [:show, :update, :destroy]

  # GET /posttests
  def index
    @posttests = Posttest.all

    render json: @posttests
  end

  # GET /posttests/1
  def show
    render json: @posttest
  end

  # POST /posttests
  def create
    @posttest = Posttest.new_post(posttest_params)
    render json: @posttest
    # @posttest = Posttest.new(posttest_params)

    # if @posttest.save
    #   render json: @posttest, status: :created, location: @posttest
    # else
    #   render json: @posttest.errors, status: :unprocessable_entity
    # end
  end

  # PATCH/PUT /posttests/1
  def update
    if @posttest.update(posttest_params)
      render json: @posttest
    else
      render json: @posttest.errors, status: :unprocessable_entity
    end
  end

  # DELETE /posttests/1
  def destroy
    @posttest.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_posttest
      @posttest = Posttest.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def posttest_params
      params.require(:posttest).permit(:title, :coontent, :image)
    end
end
