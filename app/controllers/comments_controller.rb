class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :update, :destroy]

  # GET /comments
  def index
    @comments = Comment.get_comment(params)
    render json: @comments
  end

  # GET /comments/1
  def show
    render json: @comment
  end

  # POST /comments
  def create
    @comment = Comment.new_comment(params,@user_session.user_id)
    render json: @comment
    # @comment = Comment.new(comment_params)
    # if @comment.save
    #   render json: @comment, status: :created, location: @comment
    # else
    #   render json: @comment.errors, status: :unprocessable_entity
    # end
  end

  # PATCH/PUT /comments/1
  def update
    @comment = Comment.update_comment(params,@user_session.user_id)
    render json: @comment
    # if @comment.update(comment_params)
    #   render json: @comment
    # else
    #   render json: @comment.errors, status: :unprocessable_entity
    # end
  end

  # DELETE /comments/1
  def destroy
    params[:user_id] = @user_session.user_id
    @comment = Comment.delete_comment(params)
    render json: @comment
    # @comment.destroy
    # render json: @comment
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def comment_params
      params.require(:comment).permit(:post_id, :user_id, :description, :image)
    end
end
