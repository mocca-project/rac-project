class SessionsController < ApplicationController
  skip_before_action :set_session, :only => [:create]

  # GET /sessions
  def index
    @sessions = Session.all

    render json: @sessions
  end

  # GET /sessions/1
  def show
    render json: @session
  end

  # POST /sessions
  def create
    @session = Session.new_login(session_params)    
    if !@session[:error]
      render json: @session
    else
      render json: @session,status: :unauthorized
    end
  end

  # PATCH/PUT /sessions/1
  def update
    if @session.update(session_params)
      render json: @session
    else
      render json: @session.errors, status: :unprocessable_entity
    end
  end

  # DELETE /sessions/1
  def destroy
    if @user_session.destroy
      render json: { message: "Logout Complete."}
    else
      render json: {error: @session.errors}
    end
  end

  private
    # Only allow a trusted parameter "white list" through.
    def session_params
      params.require(:session).permit(:username, :password)
    end
end
