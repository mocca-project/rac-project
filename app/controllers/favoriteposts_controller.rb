class FavoritepostsController < ApplicationController
  before_action :set_favoritepost, only: [:show, :update, :destroy]

  # GET /favoriteposts
  def index
    @favoritepost = Favoritepost.get_my_favoritepost(@user_session.user_id)
    render json: @favoritepost
  end

  # GET /favoriteposts/1
  def show
    render json: @favoritepost
  end

  # POST /favoriteposts
  def create
    @favoritepost = Favoritepost.new_favoritepost(params, @user_session.user_id)
    render json: @favoritepost
  end

  # PATCH/PUT /favoriteposts/1
  def update
    if @favoritepost.update(favoritepost_params)
      render json: @favoritepost
    else
      render json: @favoritepost.errors, status: :unprocessable_entity
    end
  end

  # DELETE /favoriteposts/1
  def destroy
    params[:user_id] = @user_session.user_id
    @favoritepost = Favoritepost.delete_favoritepost(params)
    render json: @favoritepost
    # @favoritepost.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_favoritepost
      @favoritepost = Favoritepost.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def favoritepost_params
      params.require(:favoritepost).permit(:post_id, :user_id)
    end
end
