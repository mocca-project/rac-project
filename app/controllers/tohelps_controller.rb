class TohelpsController < ApplicationController
  before_action :set_tohelp, only: [:show, :update, :destroy]

  # GET /tohelps
  def index
    @tohelp = Tohelp.get_tohelp(params, @user_session.user_id)
    render json: @tohelp
  end

  # GET /tohelps/1
  def show
    render json: @tohelp
  end

  # POST /tohelps
  def create
    params[:user_id] = @user_session.user_id
    @tohelp = Tohelp.new_tohelp(params)
    render json: @tohelp
  end

  # PATCH/PUT /tohelps/1
  def update
    if @tohelp.update(tohelp_params)
      render json: @tohelp
    else
      render json: @tohelp.errors, status: :unprocessable_entity
    end
  end

  # DELETE /tohelps/1
  def destroy
    params[:user_id] = @user_session.user_id
    @tohelp = Tohelp.delete_tohelp(params)
    render json: @tohelp
    # @tohelp.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tohelp
      @tohelp = Tohelp.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def tohelp_params
      params.require(:tohelp).permit(:post_id, :user_id, :name)
    end
end
