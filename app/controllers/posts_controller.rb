class PostsController < ApplicationController
  before_action :set_post, only: [:show, :update, :destroy]

  # GET /posts
  def index
    @posts = Post.get_all_post(params)
    render json: @posts
  end

  def showMypost
    @posts = Post.get_my_post( @user_session.user_id, params)
    render json: @posts
  end

  # GET /posts/1
  def show
    @post = Post.get_post(@post, @user_session.user_id, params)
    render json: @post
  end

  # POST /posts
  def create
    @post = Post.new_post(params, @user_session.user_id)
    render json: @post
  end

  # PATCH/PUT /posts/1
  def update
    # @post = Post.update_post(params, @user_session.user_id)
    @post = Post.update_post(params, @user_session.user_id)
    render json: @post
  end

  # DELETE /posts/1
  def destroy
    params[:user_id] = @user_session.user_id
    @post = Post.delete_post(params)
    render json: @post
    # @post.destroy
  end

  def destroyImage
    params[:image] = JSON.parse(params[:image]) if params[:image].class == String  
    params[:user_id] = @user_session.user_id
    @post = Post.delete_image(params)
    render json: @post
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def post_params
      params.require(:post).permit(:user_id, :title, :description, :type_post, :image, :identify)
    end
end
