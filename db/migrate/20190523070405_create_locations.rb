class CreateLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :locations do |t|
      t.integer :post_id
      t.string :title
      t.string :latitude
      t.string :longitude

      t.timestamps
    end
  end
end
