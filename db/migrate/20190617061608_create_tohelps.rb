class CreateTohelps < ActiveRecord::Migration[5.2]
  def change
    create_table :tohelps do |t|
      t.integer :post_id
      t.integer :user_id
      t.string :name

      t.timestamps
    end
  end
end
