class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :username
      t.string :password
      t.string :name
      t.string :email
      t.string :phone
      t.text :address
      t.string :gender
      t.string :position
      t.boolean :verify
      t.integer :role_id

      t.timestamps
    end
  end
end
