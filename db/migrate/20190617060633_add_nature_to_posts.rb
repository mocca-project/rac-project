class AddNatureToPosts < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :nature, :string
  end
end
