class AddIdentifyToPosts < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :identify, :boolean
  end
end
