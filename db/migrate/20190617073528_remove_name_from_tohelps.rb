class RemoveNameFromTohelps < ActiveRecord::Migration[5.2]
  def change
    remove_column :tohelps, :name, :string
  end
end
