# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

1.times do
  Role.create([
    { #id 1
      role: "User"
    },
    { #id 2
      role: "Volunteer"
    },
    { #id 3
      role: "Admin"
    }
  ])
  User.create([
    {
      username: "ggez123".downcase,
      password: Digest::SHA2.new(512).hexdigest("12345678"),
      name: "Rutchanai Chaiyasit",
      email: "rutchanai@gmail.com",
      phone: "0934920570",
      address: "Udonthani",
      gender: "Male",
      position: "Student",
      role_id: 3
    },
    {
      username: "user01".downcase,
      password: Digest::SHA2.new(512).hexdigest("12345678"),
      name: "Hot Hot man",
      email: "user01@gmail.com",
      phone: "0868609173",
      address: "Bangkok",
      gender: "Male",
      position: "Student",
      role_id: 1
    },
    {
      username: "user02".downcase,
      password: Digest::SHA2.new(512).hexdigest("12345678"),
      name: "Kittipong Pongfirk",
      email: "user02@gmail.com",
      phone: "0917345215",
      address: "khonkaen",
      gender: "Famale",
      position: "Doctor",
      role_id: 2
    }
  ])
  admin = User.find(1)
	admin.image.attach(io:File.open(Rails.root.join("./user.png")), filename: "image_" + Time.now.strftime("%Y%m%d%I%M%S"))
	first = User.find(2)
	first.image.attach(io:File.open(Rails.root.join("./user.png")), filename: "image_" + Time.now.strftime("%Y%m%d%I%M%S"))
	second = User.find(3)
  second.image.attach(io:File.open(Rails.root.join("./user.png")), filename: "image_" + Time.now.strftime("%Y%m%d%I%M%S"))
  Post.create([
    {
      user_id: 2,
      title: "Accident on street 8",
      description: "need some one to help!",
      type_post: "Accident",
      status: "Wait",
      nature: "Broken legs",
      identify: 1
    },
    {
      user_id: 2,
      title: "Crime! need help",
      description: "bandit steal a apple",
      type_post: "Crime",
      status: "Helping",
      identify: 1
    },
    {
      user_id: 3,
      title: "Car crash",
      description: "Injury 1 people",
      type_post: "Accident",
      status: "Finish",
      nature: "Car crash",
      identify: 0
    }
  ])

  post1 = Post.find(1)
  post1.image.attach(io:File.open(Rails.root.join("./post.png")), filename: "image_" + Time.now.strftime("%Y%m%d%I%M%S"))
  post2 = Post.find(2)
  post2.image.attach(io:File.open(Rails.root.join("./post.png")), filename: "image_" + Time.now.strftime("%Y%m%d%I%M%S"))
  post3 = Post.find(3)
  post3.image.attach(io:File.open(Rails.root.join("./post.png")), filename: "image_" + Time.now.strftime("%Y%m%d%I%M%S"))
  
  Location.create([
    {
      post_id: 1,
      title: "street 8",
      latitude: "14.055664",
      longitude: "100.564709"
    },
    {
      post_id: 2,
      title: "Crime here, come on",
      latitude: "13.780996",
      longitude: "100.523069"
    },
    {
      post_id: 3,
      title: "My location",
      latitude: "13.795927",
      longitude: "100.680493"
    }
  ])

end
