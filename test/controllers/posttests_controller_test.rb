require 'test_helper'

class PosttestsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @posttest = posttests(:one)
  end

  test "should get index" do
    get posttests_url, as: :json
    assert_response :success
  end

  test "should create posttest" do
    assert_difference('Posttest.count') do
      post posttests_url, params: { posttest: { coontent: @posttest.coontent, title: @posttest.title } }, as: :json
    end

    assert_response 201
  end

  test "should show posttest" do
    get posttest_url(@posttest), as: :json
    assert_response :success
  end

  test "should update posttest" do
    patch posttest_url(@posttest), params: { posttest: { coontent: @posttest.coontent, title: @posttest.title } }, as: :json
    assert_response 200
  end

  test "should destroy posttest" do
    assert_difference('Posttest.count', -1) do
      delete posttest_url(@posttest), as: :json
    end

    assert_response 204
  end
end
