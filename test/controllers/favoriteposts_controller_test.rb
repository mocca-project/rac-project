require 'test_helper'

class FavoritepostsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @favoritepost = favoriteposts(:one)
  end

  test "should get index" do
    get favoriteposts_url, as: :json
    assert_response :success
  end

  test "should create favoritepost" do
    assert_difference('Favoritepost.count') do
      post favoriteposts_url, params: { favoritepost: { post_id: @favoritepost.post_id, user_id: @favoritepost.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show favoritepost" do
    get favoritepost_url(@favoritepost), as: :json
    assert_response :success
  end

  test "should update favoritepost" do
    patch favoritepost_url(@favoritepost), params: { favoritepost: { post_id: @favoritepost.post_id, user_id: @favoritepost.user_id } }, as: :json
    assert_response 200
  end

  test "should destroy favoritepost" do
    assert_difference('Favoritepost.count', -1) do
      delete favoritepost_url(@favoritepost), as: :json
    end

    assert_response 204
  end
end
