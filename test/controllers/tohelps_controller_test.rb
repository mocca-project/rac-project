require 'test_helper'

class TohelpsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tohelp = tohelps(:one)
  end

  test "should get index" do
    get tohelps_url, as: :json
    assert_response :success
  end

  test "should create tohelp" do
    assert_difference('Tohelp.count') do
      post tohelps_url, params: { tohelp: { name: @tohelp.name, post_id: @tohelp.post_id, user_id: @tohelp.user_id } }, as: :json
    end

    assert_response 201
  end

  test "should show tohelp" do
    get tohelp_url(@tohelp), as: :json
    assert_response :success
  end

  test "should update tohelp" do
    patch tohelp_url(@tohelp), params: { tohelp: { name: @tohelp.name, post_id: @tohelp.post_id, user_id: @tohelp.user_id } }, as: :json
    assert_response 200
  end

  test "should destroy tohelp" do
    assert_difference('Tohelp.count', -1) do
      delete tohelp_url(@tohelp), as: :json
    end

    assert_response 204
  end
end
